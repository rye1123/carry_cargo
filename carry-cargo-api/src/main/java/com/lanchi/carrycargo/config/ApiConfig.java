package com.lanchi.carrycargo.config;

import com.lanchi.carrycargo.common.framework.api.ApiExceptionHandler;
import com.lanchi.carrycargo.common.framework.api.ApiParamInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.PostConstruct;

@Configuration
public class ApiConfig extends ResourceConfig {

    @Value("${spring.jersey.application-path}")
    private String apiPath;

    public ApiConfig() {
        packages("com.lanchi.carrycargo.api");
        register(ApiParamInterceptor.class);
        register(ApiExceptionHandler.class);
    }

    @PostConstruct
    private void init2() {
        configureSwagger();

    }

    /**
     * 配置Swagger
     */
    private void configureSwagger() {
        register(io.swagger.jaxrs.listing.ApiListingResource.class);
        register(io.swagger.jaxrs.listing.AcceptHeaderApiListingResource.class);
        register(io.swagger.jaxrs.listing.SwaggerSerializers.class);
    }

}

