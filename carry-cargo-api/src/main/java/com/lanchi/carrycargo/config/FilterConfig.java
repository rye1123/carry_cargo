package com.lanchi.carrycargo.config;

import com.lanchi.carrycargo.common.framework.api.ApiRequestFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean registerApiFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new ApiRequestFilter());
        registration.addUrlPatterns("/*");
        registration.setName("apiFilter");
        //值越小，Filter越靠前。
        registration.setOrder(1);
        return registration;
    }
}
