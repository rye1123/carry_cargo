package com.lanchi.carrycargo.config;

import com.lanchi.carrycargo.common.config.AbstractDataSourceConfig;
import lombok.Data;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;


@Data
@ConfigurationProperties(prefix = "spring.datasource.carrycargo")
@tk.mybatis.spring.annotation.MapperScan(
        basePackages ={"com.lanchi.carrycargo.dao"},
        sqlSessionFactoryRef = "carryCargoSqlSessionFactory")
@Configuration
public class CarryCargoDataSourceConfig extends AbstractDataSourceConfig {

    @Primary
    @Bean("carryCargoDataSource")
    public DataSource getDataSource() throws SQLException {
        return _getDataSource();
    }

    @Primary
    @Bean("carryCargoTransactionManager")
    public DataSourceTransactionManager transactionManager(@Qualifier("carryCargoDataSource") DataSource dataSource) throws SQLException {
        return new DataSourceTransactionManager(dataSource);
    }

    @Primary
    @Bean("carryCargoSqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(@Qualifier("carryCargoDataSource") DataSource dataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mybatis/*.xml"));
        return sessionFactory.getObject();
    }

}
