package com.lanchi.carrycargo.dto.test;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "出参")
@Data
public class TestResultDTO {

    @ApiModelProperty(value = "参数1")
    private String t1;

    @ApiModelProperty(value = "参数2")
    private Integer t2;
}
