package com.lanchi.carrycargo.api.test;

import com.lanchi.carrycargo.common.framework.api.ApiRespDTO;
import com.lanchi.carrycargo.dto.test.TestParamDTO;
import com.lanchi.carrycargo.dto.test.TestResultDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;


@Api("测试接口")
@Component
@Path("/test")
public class TestApi {
    @ApiOperation(value="接口1",notes="测试接口")
    @GET
    @Path("/t1")
    public ApiRespDTO<TestResultDTO> getOrderDetail(TestParamDTO param) {
        TestResultDTO result = new TestResultDTO();
        result.setT1(param.getUserName());
        result.setT2(Integer.valueOf(param.getAge()));
        return ApiRespDTO.buildSuccess(result);
    }
}
