package com.lanchi.carrycargo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = "com.lanchi.carrycargo",exclude = {DataSourceAutoConfiguration.class})
public class CarryCargoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarryCargoApiApplication.class, args);
	}

}
