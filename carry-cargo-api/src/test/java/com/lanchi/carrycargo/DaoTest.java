package com.lanchi.carrycargo;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanchi.carrycargo.dao.ProductDao;
import com.lanchi.carrycargo.po.ProductPo;
import com.lanchi.carrycargo.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DaoTest extends AbstractApiTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDao productDao;

    @Test
    public void testGetById() {
        ProductPo product = productService.getById(1L);
        Assert.assertNotNull(product);
    }

    @Test
    public void testLogicDelById() {
        int num = productService.logicDelById(1L);
        Assert.assertNotNull(num == 1);
    }

    @Test
    public void testUpdate() {
        ProductPo update = new ProductPo();
        update.setId(1L);
        update.setIsDelete(0);
        int num = productService.update(update);
        Assert.assertNotNull(num == 1);
    }

    @Test
    public void testQuery(){
        ProductPo param = new ProductPo();
        List<ProductPo> queryList =  productService.query(param);
        Assert.assertNotNull(queryList);
    }

    @Test
    public void testQueryPage(){
        ProductPo param = new ProductPo();
        PageInfo<ProductPo> page1 =  productService.query(1,1,param);
        Assert.assertNotNull(page1);
        PageInfo<ProductPo> page2 =  productService.query(2,1,param);
        Assert.assertNotNull(page2);
        PageInfo<ProductPo> page3 =  productService.query(3,1,param);
        Assert.assertNotNull(page3);
    }

    @Test
    public void tesetTestGetById(){
        ProductPo product =productDao.testGetById(1L);
        Assert.assertNotNull(product);
    }
}
