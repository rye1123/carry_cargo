package com.lanchi.carrycargo.validate;

import java.util.Map;


import com.lanchi.carrycargo.common.util.ValidatorUtil;

public class ValidatorTest {

    public static void main(String[] args) {
        StudentInfo s = null;
        try {
            s = StudentInfo.class.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        long startTime = System.currentTimeMillis();
        print(ValidatorUtil.validate(s));
        System.out.println("===============耗时(毫秒)=" + (System.currentTimeMillis() - startTime));

        s.setUserName("小明");
        s.setAge("a10");
        s.setBirthday("2016-9-1");
        startTime = System.currentTimeMillis();
        print(ValidatorUtil.validate(s));
        System.out.println("===============耗时(毫秒)=" + (System.currentTimeMillis() - startTime));



    }

    private static void print(Map<String,StringBuilder> errorMap){
        if(errorMap != null){
            for(Map.Entry<String, StringBuilder> m : errorMap.entrySet()){
                System.out.println(m.getKey() + ":" + m.getValue().toString());
            }
        }
    }
}