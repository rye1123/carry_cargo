<#include "/macro.include"/>
<#include "/custom.include">
<#assign className = table.className>
package ${basePackage}.bo.${table.sqlName};

import java.util.Date;
import javax.persistence.Table;
import lombok.Data;
import com.lanchi.carrycargo.common.base.po.BasePo;

/**
*
* @ClassName: ${className}
* @Description: 映射${table.sqlName}表
* @author: carme-generator
*
*/
@Data
@Table(name="${table.sqlName}")
public class ${className}Po extends BasePo<Long> {
<#list table.columns as column>
    <#if column.columnNameLower == "id"><#elseif column.columnNameLower == "isDelete"><#elseif column.columnNameLower == "gtmCreate"><#elseif column.columnNameLower == "gtmModified"><#elseif column.columnNameLower == "attribute"><#elseif column.javaType == "Boolean">
    /**
    * ${column.columnAlias}
    */
    private Integer ${column.columnNameLower};
    <#elseif column.javaType == "java.lang.String">
    /**
    * ${column.columnAlias}
    */
    private String ${column.columnNameLower};
    <#elseif column.javaType == "java.util.Date">
    /**
    * ${column.columnAlias}
    */
    private Date ${column.columnNameLower};
    <#elseif column.javaType == "java.lang.Long">
    /**
    * ${column.columnAlias}
    */
    private Long ${column.columnNameLower};
    <#elseif column.javaType == "java.lang.Integer">
    /**
    * ${column.columnAlias}
    */
    private Integer  ${column.columnNameLower};
    <#else>
    /**
    * ${column.columnAlias}
    */
    private ${column.javaType} ${column.columnNameLower};
    </#if>
</#list>
}