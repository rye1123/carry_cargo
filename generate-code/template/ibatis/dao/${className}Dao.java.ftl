<#include "/custom.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basePackage}.dao.${table.sqlName};

import com.lanchi.carrycargo.common.base.dao.BaseDao;
import ${basePackage}.po.${className}Po;

public interface ${className}Dao extends BaseDao<${className}Po>{

}