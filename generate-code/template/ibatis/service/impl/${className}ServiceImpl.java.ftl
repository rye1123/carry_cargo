<#include "/custom.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basePackage}.${table.sqlName}.impl;

import com.lanchi.carrycargo.common.base.service.BaseServiceImpl;
import com.lanchi.carrycargo.dao.${className}Dao;
import com.lanchi.carrycargo.po.${className}Po;
import com.lanchi.carrycargo.service.${className}Service;
import org.springframework.stereotype.Service;

@Service
public class ${className}ServiceImpl extends BaseServiceImpl<Long,${className}Po, ${className}Dao> implements ${className}Service {

}
