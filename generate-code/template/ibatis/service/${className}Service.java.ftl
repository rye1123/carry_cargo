<#include "/custom.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basePackage}.${table.sqlName};

import com.lanchi.carrycargo.common.base.service.BaseService;
import com.lanchi.carrycargo.po.${className}Po;

public interface ${className}Service extends BaseService<Long, ${className}Po>{

}