package com.lanchi.carrycargo.dao;

import com.lanchi.carrycargo.common.base.dao.BaseDao;
import com.lanchi.carrycargo.po.ProductPo;

public interface ProductDao extends BaseDao<ProductPo>{
    ProductPo testGetById(Long id);
}