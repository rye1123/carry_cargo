package com.lanchi.carrycargo.dao;

import com.lanchi.carrycargo.common.base.dao.BaseDao;
import com.lanchi.carrycargo.po.TaobaoStorePo;

public interface TaobaoStoreDao extends BaseDao<TaobaoStorePo>{

}