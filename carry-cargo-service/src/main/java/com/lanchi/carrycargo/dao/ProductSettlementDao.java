package com.lanchi.carrycargo.dao;

import com.lanchi.carrycargo.common.base.dao.BaseDao;
import com.lanchi.carrycargo.po.ProductSettlementPo;

public interface ProductSettlementDao extends BaseDao<ProductSettlementPo>{

}