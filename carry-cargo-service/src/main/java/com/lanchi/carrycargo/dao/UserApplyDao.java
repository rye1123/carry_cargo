package com.lanchi.carrycargo.dao;

import com.lanchi.carrycargo.common.base.dao.BaseDao;
import com.lanchi.carrycargo.po.UserApplyPo;

public interface UserApplyDao extends BaseDao<UserApplyPo>{

}