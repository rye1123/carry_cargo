package com.lanchi.carrycargo.dao;

import com.lanchi.carrycargo.common.base.dao.BaseDao;
import com.lanchi.carrycargo.po.UserPo;

public interface UserDao extends BaseDao<UserPo>{

}