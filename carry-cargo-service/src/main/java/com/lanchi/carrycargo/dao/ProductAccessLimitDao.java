package com.lanchi.carrycargo.dao;

import com.lanchi.carrycargo.common.base.dao.BaseDao;
import com.lanchi.carrycargo.po.ProductAccessLimitPo;

public interface ProductAccessLimitDao extends BaseDao<ProductAccessLimitPo>{

}