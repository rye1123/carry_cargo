package com.lanchi.carrycargo.common.base.service;

import com.github.pagehelper.PageInfo;
import com.lanchi.carrycargo.common.base.po.BasePo;

import java.util.List;

/**
 * Created by admin on 2017/4/19.
 */
public interface BaseService<PK, T extends BasePo> {
    /**
     * 根据id获得对象
     */
    T getById(PK id);

    /**
     * 根据id列表获得对象
     * @param id
     * @return
     */
    List<T> getByIds(List<PK> id);

    /**
     * 基础对象查询
     * @param query
     * @return
     */
    List<T> query(T query);

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @param query
     * @return
     */
    PageInfo<T> query(int pageNum, int pageSize, T query);

    /**
     * 保存对象
     */
    int save(T entity);

    /**
     * 更新对象
     * @param entity
     * @return
     */
    int update(T entity);

    /**
     * 根据主键id删除对象
     */
    int deleteById(PK id);

    int deleteByIds(List<PK> idList);


    /**
     * 根据主键id逻辑删除对象
     *
     * @param id
     * @return
     */
    public int logicDelById(PK id);
}

