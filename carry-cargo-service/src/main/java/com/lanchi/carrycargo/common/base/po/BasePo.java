package com.lanchi.carrycargo.common.base.po;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
public class BasePo<T> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private T id;

    /**
     * 是否删除0否1是
     */
    private Integer isDelete;


    /**
     * 创建时间
     */
    private Date gtmCreate;


    /**
     * 修改时间
     */
    private Date gtmModified;


    /**
     * 扩展
     */
    private Object attribute;
}
