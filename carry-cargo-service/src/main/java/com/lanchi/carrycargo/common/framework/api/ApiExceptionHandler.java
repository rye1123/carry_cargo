package com.lanchi.carrycargo.common.framework.api;


import com.lanchi.carrycargo.common.constants.ApiCommonConstants;
import com.lanchi.carrycargo.common.util.JsonCarmelUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.io.EOFException;

@Slf4j
@Provider
public class ApiExceptionHandler implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception e) {
        String code = ApiCommonConstants.SYS_SYSTEM_ERROR;
        String message = "";
        if(e instanceof ApiException){
            ApiException apiException = (ApiException)e;
            code = apiException.getCode();
            message = apiException.getMessage();
        }else if(e instanceof EOFException){
            message="param参数为空";
        }else{
            log.error(RequestHelper.getRequestMsg(e.getMessage()),e);
        }
        log.error(RequestHelper.getRequestMsg(e.getMessage()),e);

        ApiRespDTO resp = new ApiRespDTO();
        resp.setCode(code);
        resp.setMessage(message);
        String result = JsonCarmelUtil.toJson(resp);
        Response.ResponseBuilder responseBuilder = Response.ok(result, MediaType.APPLICATION_JSON);
        return responseBuilder.build();
    }
}