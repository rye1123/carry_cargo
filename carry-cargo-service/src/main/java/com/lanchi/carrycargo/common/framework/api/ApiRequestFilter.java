package com.lanchi.carrycargo.common.framework.api;

import org.apache.commons.lang3.StringUtils;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class ApiRequestFilter implements Filter {
    public FilterConfig config;

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        try {
            String url = getCallUrl(req);
            ApiRequest info = new ApiRequest();
            info.setIp(req.getRemoteAddr());
            info.setUrl(url);
            RequestHelper.setRequest(info);
            filterChain.doFilter(request, response);
        }finally {
            RequestHelper.destroy();
        }

    }

    @Override
    public void destroy() {

    }

    private String getCallUrl(HttpServletRequest req) {
        String url = req.getRequestURI().toString().replace(req.getContextPath(), "");
        return url;
    }


}
