package com.lanchi.carrycargo.common.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.*;

import java.text.SimpleDateFormat;

public class JsonCarmelUtil {
    private static ObjectMapper carmelMapper = new ObjectMapper();

    static {
        carmelMapper = new ObjectMapper();
        //空对象不出错
        carmelMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        //忽略未知属性
        carmelMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //驼峰属性转下划线属性
        //carmelMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        //数据类型转双引号
        carmelMapper.configure(JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS, true);
        //重写null serial实现方式
        carmelMapper.setSerializerFactory(carmelMapper.getSerializerFactory()
                .withSerializerModifier(new MyJacksonSerializer()));

        //日期格式定义
        carmelMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    }

    public static ObjectMapper getCarmelMapper() {
        return carmelMapper;
    }

    /**
     * json字符串转换成对象,不显式抛出异常
     * @param json
     * @param clazz
     * @return
     */
    public static <T> T fromJSON(String json, Class<T> clazz) {
        try {
            return getCarmelMapper().readValue(json, clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 对象转换成json字符串,不显式抛出异常
     * @param object
     * @return
     */
    public static String toJson(Object object) {
        try {
            return getCarmelMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
