package com.lanchi.carrycargo.common.framework.api;

import org.apache.commons.lang3.StringUtils;

public class RequestHelper {
    private static ThreadLocal<ApiRequest> requestLocal = new ThreadLocal<ApiRequest>();

    public static void setRequest(ApiRequest info) {
        requestLocal.set(info);
    }

    public static ApiRequest getRequest() {
        return requestLocal.get();
    }

    public static void destroy(){
        requestLocal.remove();
    }

    public static String getRequestMsg() {
        return getRequestMsg("");
    }

    public static String getRequestMsg(String message) {
        ApiRequest req = getRequest();
        StringBuilder builder = new StringBuilder();
        builder.append("\nurl:").append(req.getUrl());
        builder.append("\nparam:").append(req.getParam());
        if(StringUtils.isNotBlank(req.getResult())) {
            builder.append("\nresult:").append(req.getResult());
        }
        if (StringUtils.isNotBlank(message)) {
            builder.append("\nmessage:").append(message);
        }
        return builder.toString();
    }
}
