package com.lanchi.carrycargo.common.constants;

/**
 * Created by huangkl on 2018/5/2.
 */
public  class CommonConstants {
    /**
     * 是
     */
    public final static Integer TRUE     = 1;

    /**
     * 否
     */
    public final static Integer FALSE    = 0;

    /**
     * 成功标识
     */
    public final static String  SUCCESS  = "0";

    /**
     * 失败标识
     */
    public final static String  ERROR    = "-1";

    public final static String  ENCODING = "utf-8";

    public final static Integer NORMAL   = 0;

    public final static Integer DELETE   = 1;

}
