package com.lanchi.carrycargo.common.base.dao;

import tk.mybatis.mapper.additional.insert.InsertListMapper;
import tk.mybatis.mapper.annotation.RegisterMapper;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.rowbounds.SelectByConditionRowBoundsMapper;

@RegisterMapper
public interface BaseDao<T> extends Mapper<T>, IdsMapper<T>, ConditionMapper<T>, SelectByConditionRowBoundsMapper<T>, InsertListMapper<T> {
}
