package com.lanchi.carrycargo.common.framework.api;

import com.lanchi.carrycargo.common.constants.ApiCommonConstants;

import java.io.Serializable;

public class ApiRespDTO<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ApiRespDTO() {

    }

    public static ApiRespDTO buildSuccess() {
        return buildSuccess(null);
    }

    public static ApiRespDTO buildSuccess(Object result) {
        ApiRespDTO api = new ApiRespDTO();
        api.setResult(result);
        return api;
    }

    public static ApiRespDTO buildError(String code, String message) {
        return buildError(code, message, null);
    }

    public static ApiRespDTO buildError(String code, String message, Object result) {
        ApiRespDTO api = new ApiRespDTO();
        api.setCode(code);
        api.setMessage(message);
        api.setResult(result);
        return api;
    }

    public ApiRespDTO(T result) {
        this.result = result;
    }

    public String code = ApiCommonConstants.SUCCESS;

    private String message = "";

    private T result;

    public boolean success() {
        return ApiCommonConstants.SUCCESS.equals(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

}
