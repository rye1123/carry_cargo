package com.lanchi.carrycargo.common.framework.api;

import com.lanchi.carrycargo.common.constants.ApiCommonConstants;
import com.lanchi.carrycargo.common.constants.CommonConstants;
import com.lanchi.carrycargo.common.util.JsonCarmelUtil;
import com.lanchi.carrycargo.common.util.ValidatorUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class ApiParamInterceptor implements ReaderInterceptor, WriterInterceptor {
    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
        byte[] buffer = IOUtils.toByteArray(context.getInputStream());
        String body = new String(buffer, CommonConstants.ENCODING);
        log.debug("request param body:{}", body);
        Object param = null;
        if (StringUtils.isNoneBlank(body)) {
            param = JsonCarmelUtil.fromJSON(body, context.getType());
            //threadLocal 设置 api 入参
            RequestHelper.getRequest().setParam(body);
            //检查参数
            checkParam(param);
            return param;
        } else {
            return context.proceed();
        }
    }

    /**
     * 检查参数
     *
     * @param param
     */
    private void checkParam(Object param) {
        if (!(param instanceof ApiParamDTO)) {
            throw new ApiException(ApiCommonConstants.SYS_SYSTEM_ERROR, "param must be ApiParamDTO");
        }
        Map<String, StringBuilder> errorMap = ValidatorUtil.validate(param);
        if (errorMap.size() > 0) {
            throw new ApiException(ApiCommonConstants.SYS_PARAM_CHECK_ERROR, convertStr(errorMap));
        }
    }

    private String convertStr(Map<String, StringBuilder> errorMap) {
        StringBuilder result = new StringBuilder();
        for (String key : errorMap.keySet()) {
            if (result.length() != 0) {
                result.append(",");
            }
            result.append(key).append(":").append(errorMap.get(key));
        }
        return result.toString();
    }


    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        Object resultObject = context.getEntity();
        if (resultObject == null) {
            return;
        }
        if(RequestHelper.getRequest().getUrl().indexOf("/swagger.json")!=-1){
            context.proceed();
            return;
        }
        String body = "";
        if (resultObject.getClass().equals(String.class)) {
            body = resultObject.toString();
        }
        else {
            if (!(resultObject instanceof ApiRespDTO)) {
                throw new ApiException(ApiCommonConstants.SYS_SYSTEM_ERROR, "result must be ApiRespDTO");
            }
            ApiRespDTO<?> result = (ApiRespDTO<?>) resultObject;
            body = JsonCarmelUtil.toJson(result);
        }
        //threadLocal 设置 api 出参
        RequestHelper.getRequest().setResult(body);
        context.getOutputStream().write(body.getBytes(CommonConstants.ENCODING));
    }
}
