package com.lanchi.carrycargo.common.base.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanchi.carrycargo.common.constants.CommonConstants;
import com.lanchi.carrycargo.common.base.dao.BaseDao;
import com.lanchi.carrycargo.common.base.po.BasePo;
import com.lanchi.carrycargo.common.base.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by admin on 2017/4/19.
 */
public abstract class BaseServiceImpl<PK, T extends BasePo,D extends BaseDao> implements BaseService<PK, T> {

    @Autowired
    private D dao;

    private BaseDao getDao(){
        return dao;
    }

    /**
     * 根据id获得对象
     */
    @Override
    public T getById(PK id) {
        return (T) getDao().selectByPrimaryKey(id);
    }

    /**
     * 根据id列表获得对象
     *
     * @param idList
     * @return
     */
    public List<T> getByIds(List<PK> idList) {
        return getDao().selectByIds(list2Str(idList));
    }


    /**
     * 查询列表
     *
     * @param query
     * @return
     */
    public List<T> query(T query) {
        return getDao().select(query);
    }

    public PageInfo<T> query(int pageNum,int pageSize,T query) {
        PageHelper.startPage(pageNum,pageSize);
        List<T> list = getDao().select(query);
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        return pageInfo;
    }

    /**
     * 保存对象
     */
    @Override
    public int save(T entity) {
        return getDao().insert(entity);
    }


    @Override
    public int update(T entity) {
        return getDao().updateByPrimaryKeySelective(entity);
    }


    /**
     * 根据主键id删除对象
     */
    public int deleteById(PK id) {
        return getDao().deleteByPrimaryKey(id);
    }

    /**
     * 根据主键id批量删除对象
     */
    public int deleteByIds(List<PK> idList) {
        return getDao().deleteByIds(list2Str(idList));
    }

    /**
     * 根据主键id逻辑删除对象
     *
     * @param id
     * @return
     */
    public int logicDelById(PK id) {
        BasePo update = null;
        try {
            update = getPoClass().newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        update.setId(id);
        update.setIsDelete(CommonConstants.TRUE);
        return getDao().updateByPrimaryKeySelective(update);
    }


    private String list2Str(List<PK> idList) {
        StringBuilder ids = new StringBuilder();
        for (PK id : idList) {
            if (ids.length() != 0) {
                ids.append(",");
            }
            ids.append(id);
        }
        return ids.toString();
    }


    public Class<T> getPoClass() {
        Class<T> tClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        return tClass;
    }
}
