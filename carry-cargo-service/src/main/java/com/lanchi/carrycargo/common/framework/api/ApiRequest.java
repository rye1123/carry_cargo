package com.lanchi.carrycargo.common.framework.api;

import lombok.Data;

@Data
public class ApiRequest {

    /**
     * ip地址
     */
    private String ip;

    /**
     * url地址
     */
    private String url;

    /**
     * 参数
     */
    private String param;

    /**
     * 返回结果
     */
    private String result;

    /**
     * 用户Id
     */
    private Long userId;

}
