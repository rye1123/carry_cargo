package com.lanchi.carrycargo.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * JSON转换工具类
 * 
 * @date 2010-11-23
 * @since JDK 1.5
 * @author dingkui
 * @version 1.0
 * 
 */
public class JsonUtil {
    /**
     * 
     */
    public static ObjectMapper mapper;


    static {
        mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        //重写null serial实现方式
        mapper.setSerializerFactory(mapper.getSerializerFactory().withSerializerModifier(new MyJacksonSerializer()));
    }

    /**
     * 将object转为json字符串
     * 
     * @param pObj
     *            参数
     * @throws IOException
     *             异常
     * @throws JsonMappingException
     *             异常
     * @throws JsonGenerationException
     *             异常
     * @return String
     */
    public static String writeValue(Object pObj) throws JsonGenerationException,
                                                JsonMappingException, IOException {
        return JsonUtil.mapper.writeValueAsString(pObj);
    }

    /**
     * 将content转为java object
     * 
     * @param <T>
     *            参数
     * @param pContent
     *            参数
     * @param pObj
     *            参数
     * @throws JsonParseException
     *             异常
     * @throws JsonMappingException
     *             异常
     * @throws IOException
     *             异常
     * @return Object
     */
    @SuppressWarnings("unchecked")
    public static <T> Object readValue(String pContent, Object pObj) throws JsonParseException,
                                                                    JsonMappingException,
                                                                    IOException {
        return JsonUtil.mapper.readValue(pContent, (Class<T>) pObj);
    }

    /**
     * 对象转换成json字符串,不显式抛出异常
     * @param object
     * @return
     */
    public static String toJson(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * json字符串转换成对象,不显式抛出异常
     * @param json
     * @param clazz
     * @return
     */
    public static <T> T fromJSON(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * json字符串转换成ArrayList,不显式抛出异常
     * @param json
     * @param clazz
     * @return
     */
    public static <T> List<T> fromJSONList(String json, Class<T> clazz) {
        try {
            JavaType javaType = getCollectionType(ArrayList.class, clazz);
            return mapper.readValue(json, javaType);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
        return mapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }

}
