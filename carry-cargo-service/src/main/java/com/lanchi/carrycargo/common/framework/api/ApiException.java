package com.lanchi.carrycargo.common.framework.api;

/**
 * @author kelei.huang
 * @Type ManagerException
 * @Desc cms共用异常类
 * @date 2012-7-3
 * @Version V1.0
 */
@SuppressWarnings("serial")
public class ApiException extends RuntimeException {

    protected String code;

    protected String message;

    public ApiException(String code) {
        super(code);
        this.code = code;
    }


    public ApiException(String code, String message) {
        super(code + ":" + message);
        this.code = code;
        this.message = message;
    }

    public ApiException(String code, String message, Throwable cause) {
        super(code + ":" + message, cause);
        this.code = code;
        this.message = message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
