package com.lanchi.carrycargo.common.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class MyJacksonSerializer extends BeanSerializerModifier{
    private static JsonSerializer<Object> nullArraySerializer = new MyNullArraySerializer();
    private static JsonSerializer<Object> nullObjectSerializer = new MyNullObjectSerializer();
    private static JsonSerializer<Object> nullStringSerializer = new MyNullStringSerializer();
    private static JsonSerializer<Object> myJsonSerializer = new MyJsonTrimSerializer();

    @Override
    public List<BeanPropertyWriter> changeProperties(SerializationConfig config, BeanDescription beanDesc, List<BeanPropertyWriter> beanProperties) {
        for (BeanPropertyWriter writer : beanProperties) {
            if (isArrayType(writer)) {
                writer.assignNullSerializer(nullArraySerializer);
            }else if(isObject(writer)){
                writer.assignNullSerializer(nullObjectSerializer);
            }else{
                writer.assignSerializer(myJsonSerializer);
                writer.assignNullSerializer(nullStringSerializer);
            }
        }
        return beanProperties;
    }


    public boolean isArrayType(BeanPropertyWriter writer) {
        Class<?> clazz = writer.getPropertyType();
        return clazz.isArray() || clazz.equals(List.class) || clazz.equals(Set.class);

    }

    public boolean isObject(BeanPropertyWriter writer){
        Class<?> clazz = writer.getPropertyType();
        return !ObjectUtil.isBaseObject(clazz);
    }

}

class MyNullArraySerializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        if (value == null) {
            jgen.writeStartArray();
            jgen.writeEndArray();
        } else {
            jgen.writeObject(value);
        }
    }
}

class MyNullObjectSerializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        if (value == null) {
            jgen.writeStartObject();
            jgen.writeEndObject();
        } else {
            jgen.writeObject(value);
        }
    }
}

class MyNullStringSerializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        if (value == null) {
            jgen.writeString("");
        } else {
            if(value instanceof String){
                jgen.writeObject(((String) value).trim());
            }else {
                jgen.writeObject(value);
            }
        }
    }
}

class MyJsonTrimSerializer extends JsonSerializer<Object> {
    @Override
    public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        if (value != null && value instanceof String) {
            jgen.writeObject(((String) value).trim());
        } else {
            jgen.writeObject(value);
        }
    }
}