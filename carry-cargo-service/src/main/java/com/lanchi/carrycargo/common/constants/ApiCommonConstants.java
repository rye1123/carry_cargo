package com.lanchi.carrycargo.common.constants;

public class ApiCommonConstants {

    public final static String RESULT_TEMPLATE= "{\"code\":\"%s\",\"message\":\"%s\",\"result\":{}}";

    /**************************公共编码[start]******************************/

    public static final String SUCCESS                            = "0";

    /**
     * 系统错误
     */
    public static final String SYS_SYSTEM_ERROR                   = "100001";

    /**
     * 参数检查失败
     */
    public static final String SYS_PARAM_CHECK_ERROR              = "100002";

    /**
     * token验证失败
     */
    public static final String SYS_TOKEN_AUTH_ERROR               = "100003";

    /**
     * App系统版本过低
     */
    public static final String SYS_MOBILE_VERSION_ERROR           = "100004";

    /**
     * 系统维护中
     */
    public static final String SYS_SYSTEM_REPAIR_ERROR            = "100005";

    /**
     * 签名验证失败
     */
    public static final String SYS_SIGN_ERROR                     = "100006";

    /**
     * 接口权限认证失败
     */
    public static final String SYS_POWER_ERROR                    = "100007";


}
