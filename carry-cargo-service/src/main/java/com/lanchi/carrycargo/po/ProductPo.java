package com.lanchi.carrycargo.po;

import java.util.Date;
import javax.persistence.Table;
import lombok.Data;
import com.lanchi.carrycargo.common.base.po.BasePo;

/**
*
* @ClassName: Product
* @Description: 映射product表
* @author: carme-generator
*
*/
@Data
@Table(name="product")
public class ProductPo extends BasePo<Long> {
    /**
    * 店铺id
    */
    private Long storeId;
    /**
    * 商品链接
    */
    private String productLink;
    /**
    * 类目
    */
    private String categoryName;
    /**
    * 商品名称
    */
    private String productName;
    /**
    * 商品主图
    */
    private String mainPicUrl;
    /**
    * 营销图
    */
    private String marketingPicUrl;
    /**
    * 相关商品图
    */
    private String relatePicUrl;
    /**
    * 库存
    */
    private Long productStock;
    /**
    * 券类型
    */
    private Integer  couponType;
    /**
    * 券链接
    */
    private String couponLink;
    /**
    * 券总量
    */
    private Long couponsTotal;
    /**
    * 券剩余量
    */
    private Long couponBalance;
    /**
    * 券后价
    */
    private Long couponPrice;
    /**
    * 亮点
    */
    private String highlights;
    /**
    * 特殊活动
    */
    private String specialEvent;
    /**
    * 佣金类型
    */
    private Integer  commissionType;
    /**
    * 佣金比例
    */
    private Integer  commissionRate;
    /**
    * 定向地址
    */
    private String directedAddress;
    /**
    * 团长链接
    */
    private String headLink;
    /**
    * 视频素材
    */
    private String videoFootage;
    /**
    * 样品
    */
    private String inspectionSamples;
    /**
    * 状态1未投放2在线3下架4草稿5回收站
    */
    private Integer  status;
    /**
    * 发布时间
    */
    private Date releaseTime;
    /**
    * 是否公开0否1是
    */
    private Integer  isPublic;
    /**
    * 秘钥
    */
    private String secretKey;
    /**
    * 总预算
    */
    private Long budgetAmount;
    /**
    * 总支出
    */
    private Long expensesAmount;
    /**
    * 发布状态1立即2定时3草稿
    */
    private Integer  releaseStatus;
}