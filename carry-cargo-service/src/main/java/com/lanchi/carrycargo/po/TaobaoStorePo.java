package com.lanchi.carrycargo.po;

import java.util.Date;
import javax.persistence.Table;
import lombok.Data;
import com.lanchi.carrycargo.common.base.po.BasePo;

/**
*
* @ClassName: TaobaoStore
* @Description: 映射taobao_store表
* @author: carme-generator
*
*/
@Data
@Table(name="taobao_store")
public class TaobaoStorePo extends BasePo<Long> {
    /**
    * 店铺名称
    */
    private String storeName;
    /**
    * 宝贝描述评分
    */
    private Double productDescription;
    /**
    * 商家服务
    */
    private Double sellerServices;
    /**
    * 物流服务
    */
    private Double logisticsServices;
}