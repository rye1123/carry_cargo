package com.lanchi.carrycargo.po;

import java.util.Date;
import javax.persistence.Table;
import lombok.Data;
import com.lanchi.carrycargo.common.base.po.BasePo;

/**
*
* @ClassName: ProductAccessLimit
* @Description: 映射product_access_limit表
* @author: carme-generator
*
*/
@Data
@Table(name="product_access_limit")
public class ProductAccessLimitPo extends BasePo<Long> {
    /**
    * 销售额
    */
    private Long sales;
    /**
    * 订单数量
    */
    private Integer  orderQuantity;
    /**
    * 佣金
    */
    private Long commission;
    /**
    * 粉丝点赞
    */
    private Long clickNum;
    /**
    * 累计粉丝
    */
    private Long fansNum;
    /**
    * 是否审核0否1是
    */
    private Integer  isReview;
    /**
    * 是否要求公开作品0否1是
    */
    private Integer  isPublicWorks;
    /**
    * 达人限制
    */
    private Integer  reachLimit;
}