package com.lanchi.carrycargo.po;

import java.util.Date;
import javax.persistence.Table;
import lombok.Data;
import com.lanchi.carrycargo.common.base.po.BasePo;

/**
*
* @ClassName: User
* @Description: 映射user表
* @author: carme-generator
*
*/
@Data
@Table(name="user")
public class UserPo extends BasePo<Long> {
    /**
    * 是否商家0否1是
    */
    private String isBussiness;
    /**
    * 是否达人0否1是
    */
    private String isTalent;
    /**
    * 达人id，关联小火龙用户id
    */
    private Long talentId;
}