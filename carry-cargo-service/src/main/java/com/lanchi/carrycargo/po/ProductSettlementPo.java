package com.lanchi.carrycargo.po;

import java.util.Date;
import javax.persistence.Table;
import lombok.Data;
import com.lanchi.carrycargo.common.base.po.BasePo;

/**
*
* @ClassName: ProductSettlement
* @Description: 映射product_settlement表
* @author: carme-generator
*
*/
@Data
@Table(name="product_settlement")
public class ProductSettlementPo extends BasePo<Long> {
    /**
    * 达人id
    */
    private Long userId;
    /**
    * 补贴方案
    */
    private Integer  subsidyScheme;
    /**
    * 补贴金额
    */
    private Long subsidyAmount;
    /**
    * 结算周期
    */
    private Integer  settlementCycle;
}