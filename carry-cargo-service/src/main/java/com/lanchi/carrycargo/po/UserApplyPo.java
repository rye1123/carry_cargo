package com.lanchi.carrycargo.po;

import java.util.Date;
import javax.persistence.Table;
import lombok.Data;
import com.lanchi.carrycargo.common.base.po.BasePo;

/**
*
* @ClassName: UserApply
* @Description: 映射user_apply表
* @author: carme-generator
*
*/
@Data
@Table(name="user_apply")
public class UserApplyPo extends BasePo<Long> {
    /**
    * 商品id
    */
    private Long productId;
    /**
    * 达人id
    */
    private Long userId;
    /**
    * 是否通过申请0否1是
    */
    private Integer  isApply;
    /**
    * 是否寄样
    */
    private Integer  isSendSamples;
    /**
    * 物流信息
    */
    private String logisticsInformation;
    /**
    * 审核时间
    */
    private Date reviewTime;
    /**
    * 审核状态0待审1已通过
    */
    private Integer  reviewStatus;
    /**
    * 审核人
    */
    private String reviewer;
}