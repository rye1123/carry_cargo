package com.lanchi.carrycargo.service;

import com.lanchi.carrycargo.common.base.service.BaseService;
import com.lanchi.carrycargo.po.UserPo;

public interface UserService extends BaseService<Long, UserPo>{

}