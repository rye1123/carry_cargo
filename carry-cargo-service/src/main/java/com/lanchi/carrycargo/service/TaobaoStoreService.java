package com.lanchi.carrycargo.service;

import com.lanchi.carrycargo.common.base.service.BaseService;
import com.lanchi.carrycargo.po.TaobaoStorePo;

public interface TaobaoStoreService extends BaseService<Long, TaobaoStorePo>{

}