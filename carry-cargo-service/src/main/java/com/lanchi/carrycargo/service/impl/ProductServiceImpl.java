package com.lanchi.carrycargo.service.impl;

import com.lanchi.carrycargo.common.base.service.BaseServiceImpl;
import com.lanchi.carrycargo.dao.ProductDao;
import com.lanchi.carrycargo.po.ProductPo;
import com.lanchi.carrycargo.service.ProductService;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Long,ProductPo, ProductDao> implements ProductService {

}
