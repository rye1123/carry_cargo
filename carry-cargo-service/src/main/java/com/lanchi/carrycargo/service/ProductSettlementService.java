package com.lanchi.carrycargo.service;

import com.lanchi.carrycargo.common.base.service.BaseService;
import com.lanchi.carrycargo.po.ProductSettlementPo;

public interface ProductSettlementService extends BaseService<Long, ProductSettlementPo>{

}