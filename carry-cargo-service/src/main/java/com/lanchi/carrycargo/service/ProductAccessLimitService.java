package com.lanchi.carrycargo.service;

import com.lanchi.carrycargo.common.base.service.BaseService;
import com.lanchi.carrycargo.po.ProductAccessLimitPo;

public interface ProductAccessLimitService extends BaseService<Long, ProductAccessLimitPo>{

}