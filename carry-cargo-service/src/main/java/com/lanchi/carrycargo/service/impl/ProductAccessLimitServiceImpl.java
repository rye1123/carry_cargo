package com.lanchi.carrycargo.service.impl;

import com.lanchi.carrycargo.common.base.service.BaseServiceImpl;
import com.lanchi.carrycargo.dao.ProductAccessLimitDao;
import com.lanchi.carrycargo.po.ProductAccessLimitPo;
import com.lanchi.carrycargo.service.ProductAccessLimitService;
import org.springframework.stereotype.Service;

@Service
public class ProductAccessLimitServiceImpl extends BaseServiceImpl<Long,ProductAccessLimitPo, ProductAccessLimitDao> implements ProductAccessLimitService {

}
