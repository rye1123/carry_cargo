package com.lanchi.carrycargo.service.impl;

import com.lanchi.carrycargo.common.base.service.BaseServiceImpl;
import com.lanchi.carrycargo.dao.UserDao;
import com.lanchi.carrycargo.po.UserPo;
import com.lanchi.carrycargo.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BaseServiceImpl<Long,UserPo, UserDao> implements UserService {

}
