package com.lanchi.carrycargo.service;

import com.lanchi.carrycargo.common.base.service.BaseService;
import com.lanchi.carrycargo.po.ProductPo;

public interface ProductService extends BaseService<Long, ProductPo>{

}