package com.lanchi.carrycargo.service.impl;

import com.lanchi.carrycargo.common.base.service.BaseServiceImpl;
import com.lanchi.carrycargo.dao.UserApplyDao;
import com.lanchi.carrycargo.po.UserApplyPo;
import com.lanchi.carrycargo.service.UserApplyService;
import org.springframework.stereotype.Service;

@Service
public class UserApplyServiceImpl extends BaseServiceImpl<Long,UserApplyPo, UserApplyDao> implements UserApplyService {

}
