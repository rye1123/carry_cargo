package com.lanchi.carrycargo.service.impl;

import com.lanchi.carrycargo.common.base.service.BaseServiceImpl;
import com.lanchi.carrycargo.dao.TaobaoStoreDao;
import com.lanchi.carrycargo.po.TaobaoStorePo;
import com.lanchi.carrycargo.service.TaobaoStoreService;
import org.springframework.stereotype.Service;

@Service
public class TaobaoStoreServiceImpl extends BaseServiceImpl<Long,TaobaoStorePo, TaobaoStoreDao> implements TaobaoStoreService {

}
