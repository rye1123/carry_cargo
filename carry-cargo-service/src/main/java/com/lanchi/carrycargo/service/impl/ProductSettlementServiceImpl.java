package com.lanchi.carrycargo.service.impl;

import com.lanchi.carrycargo.common.base.service.BaseServiceImpl;
import com.lanchi.carrycargo.dao.ProductSettlementDao;
import com.lanchi.carrycargo.po.ProductSettlementPo;
import com.lanchi.carrycargo.service.ProductSettlementService;
import org.springframework.stereotype.Service;

@Service
public class ProductSettlementServiceImpl extends BaseServiceImpl<Long,ProductSettlementPo, ProductSettlementDao> implements ProductSettlementService {

}
